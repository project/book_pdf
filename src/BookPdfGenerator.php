<?php

namespace Drupal\book_pdf;

use Drupal\book\BookExport;
use Drupal\book_pdf\Event\BookPdfPreGenerateEvent;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\State\StateInterface;
use Drupal\node\Entity\Node;
use mikehaertl\wkhtmlto\Pdf;
use Psr\Log\LoggerInterface;

/**
 * BookPdfGenerator service.
 */
class BookPdfGenerator {

  const CACHE_DIR = 'public://book_pdf/';

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The Book export service.
   *
   * @var \Drupal\book\BookExport
   */
  protected $bookExport;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The book_pdf logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The The book_pdf settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;


  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * Constructs a BookPdfGenerator object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\book\BookExport $book_export
   *   The Book export service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Psr\Log\LoggerInterface $logger_channel
   *   The logger.channel.book_pdf service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(StateInterface $state, BookExport $book_export, RendererInterface $renderer, LoggerInterface $logger_channel, ConfigFactoryInterface $config_factory, ContainerAwareEventDispatcher $event_dispatcher) {
    $this->state = $state;
    $this->bookExport = $book_export;
    $this->renderer = $renderer;
    $this->logger = $logger_channel;
    $this->settings = $config_factory->get('book_pdf.settings');
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Get the Book PDF cache filename.
   *
   * @param \Drupal\node\Entity\Node $book
   *   The book to get the cache filename for.
   *
   * @return string
   *   The filename.
   */
  protected function getCacheFilename(Node $book) {
    return $book->book['bid'] . '.pdf';
  }

  /**
   * Get the Book PDF contents.
   *
   * @param \Drupal\node\Entity\Node $book
   *   The Book node.
   *
   * @return bool|string
   *   The PDF contents or FALSE if there was an error during generation.
   */
  protected function getPdfContents(Node $book) {
    $defaultOptions = [];
    $basicUser = $this->settings->get('basic_user');
    $basicPass = $this->settings->get('basic_pass');
    if (!empty($basicUser) && !empty($basicPass)) {
      $defaultOptions['username'] = $basicUser;
      $defaultOptions['password'] = $basicPass;
    }
    $pdf = new Pdf(['encoding' => 'utf-8']);
    $event = $this->eventDispatcher->dispatch(new BookPdfPreGenerateEvent($book, $pdf, $defaultOptions), 'book_pdf.pre');
    $pdf->setOptions($event->getGlobalOptions());
    if (($content = $pdf->toString())) {
      return $content;
    }
    else {
      $this->logger->error('Error generating Book PDF. <br> %error', ['%error' => $pdf->getError()]);
      return FALSE;
    }
  }

  /**
   * Get the state key.
   *
   * @param \Drupal\node\Entity\Node $book
   *   The book to get the cache state key for.
   *
   * @return string
   *   The state key.
   */
  public function getStateKey(Node $book) {
    return 'book_pdf_' . $book->book['bid'];
  }

  /**
   * Get the URI for a Book's cached PDF.
   *
   * @param \Drupal\node\Entity\Node $book
   *   The Book to return the PDF file path for.
   *
   * @return bool|string
   *   The URI if the file can generated\retrieved from cache or
   *   FALSE if something went wrong.
   */
  public function getFileUri(Node $book) {
    if (\Drupal::currentUser()->hasPermission('bypass pdf caching') == FALSE) {
      $bookCached = $this->state->get($this->getStateKey($book), FALSE);
      if ($bookCached && file_exists($bookCached)) {
        return $bookCached;
      }
    }
    if ($pdfContents = $this->getPdfContents($book)) {
      if (!is_dir($this::CACHE_DIR) && !mkdir($this::CACHE_DIR)) {
        $this->logger->error('Error creating Book PDF cache directory.');
        return FALSE;
      }
      $fileName = $this::CACHE_DIR . $this->getCacheFilename($book);
      if (file_put_contents($fileName, $pdfContents)) {
        $this->state->set($this->getStateKey($book), $fileName);
        return $fileName;
      }
      else {
        $this->logger->error('Error saving cached book PDF to disk.');
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

}
