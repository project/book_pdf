<?php

namespace Drupal\book_pdf\EventSubscriber;

use Drupal\book\BookExport;
use Drupal\book_pdf\Event\BookPdfEvents;
use Drupal\book_pdf\Event\BookPdfPreGenerateEvent;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Book PDF event subscriber.
 */
class BookPdfPreGenerateSubscriber implements EventSubscriberInterface {

  /**
   * The Book export service.
   *
   * @var \Drupal\book\BookExport
   */
  protected $bookExport;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The book_pdf logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\book\BookExport $bookExport
   *   The Book export service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger factory service.
   */
  public function __construct(BookExport $bookExport, RendererInterface $renderer, LoggerChannelFactoryInterface $loggerFactory) {
    $this->bookExport = $bookExport;
    $this->renderer = $renderer;
    $this->logger = $loggerFactory->get('book_pdf');
  }

  /**
   * Adds the main book body to the PDF being generated.
   *
   * @param \Drupal\book_pdf\Event\BookPdfPreGenerateEvent $event
   *   The Book Pdf Pre Generate Event Object.
   *
   * @return \Drupal\book_pdf\Event\BookPdfPreGenerateEvent|false
   *   Event Object.
   */
  public function addBookHtml(BookPdfPreGenerateEvent $event) {
    $book = $event->getBook();
    try {
      $bookBuild = $this->bookExport->bookExportHtml($book);
      $bookHtml = $this->renderer->render($bookBuild);
    }
    catch (\Exception $e) {
      $this->logger->error('Error rendering Book export HTML. <br> %error', ['%error' => $e->getMessage()]);
      return FALSE;
    }
    $pdf = $event->getPdf();
    $pdf->addPage($bookHtml);

    return $event;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BookPdfEvents::PRE => ['addBookHtml'],
    ];
  }

}
