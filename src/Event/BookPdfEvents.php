<?php

namespace Drupal\book_pdf\Event;

/**
 * Defines events for Book PDF.
 */
final class BookPdfEvents {

  /**
   * Dispatched before book_pdf attempts to generate a PDF from the book HTML.
   *
   * The event listener method receives a
   * Drupal\book_pdf\Event\BookPdfPreStringEvent instance.
   *
   * @Event
   *
   * @var string
   */
  const PRE = 'book_pdf.pre';

}
