<?php

namespace Drupal\book_pdf\Event;

use Symfony\Contracts\EventDispatcher\Event;
use mikehaertl\wkhtmlto\Pdf;
use Drupal\node\Entity\Node;

/**
 * Class BookPdfPreStringEvent.
 *
 * @package Drupal\book_pdf\Event
 */
class BookPdfPreGenerateEvent extends Event {

  /**
   * The WKHTMLTOPDF object.
   *
   * @var \mikehaertl\wkhtmlto\Pdf
   */
  protected $pdf;

  /**
   * The WKHTMLTOPDF global options.
   *
   * @var array
   */
  protected $globalOptions;

  /**
   * The Book node.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected $book;

  /**
   * BookPdfPreStringEvent constructor.
   *
   * @param \Drupal\node\Entity\Node $book
   *   The Book node.
   * @param \mikehaertl\wkhtmlto\Pdf $pdf
   *   The WKHTMLTOPDF object.
   * @param array $default_options
   *   The default WKHTMLTOPDF global options.
   */
  public function __construct(Node $book, Pdf $pdf, array $default_options) {
    $this->pdf = $pdf;
    $this->globalOptions = $default_options;
    $this->book = $book;
  }

  /**
   * Get the PDF object.
   *
   * @return \mikehaertl\wkhtmlto\Pdf
   *   The WKHTMLTOPDF object.
   */
  public function getPdf() {
    return $this->pdf;
  }

  /**
   * Get the global WKHTMLTOPDF options.
   *
   * @return array
   *   The default options.
   */
  public function getGlobalOptions() {
    return $this->globalOptions;
  }

  /**
   * Set the global WKHTMLTOPDF options.
   *
   * @param array $options
   *   The new global options.
   */
  public function setGlobalOptions(array $options) {
    $this->globalOptions = $options;
  }

  /**
   * Get the Book node.
   *
   * @return \Drupal\node\Entity\Node
   *   The Book node.
   */
  public function getBook() {
    return $this->book;
  }

}
