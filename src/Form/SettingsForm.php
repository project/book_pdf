<?php

namespace Drupal\book_pdf\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Book PDF settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'book_pdf_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['book_pdf.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['basic_auth'] = [
      '#type' => 'fieldset',
      '#title' => 'Basic authentication',
      '#description' => 'Configure the Basic auth username and password phpwkhtmltopdf should use fetching remote assets while generating the PDF',
    ];
    $form['basic_auth']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $this->config('book_pdf.settings')->get('basic_user'),
    ];
    $form['basic_auth']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $this->config('book_pdf.settings')->get('basic_pass'),
    ];
    $form['file_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PDF file prefix'),
      '#default_value' => $this->config('book_pdf.settings')->get('file_prefix'),
      '#description' => 'Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.',
      '#element_validate' => [[static::class, 'validateFilePrefixElement']],

    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('book_pdf.settings')
      ->set('basic_user', $form_state->getValue('username'))
      ->set('basic_pass', $form_state->getValue('password'))
      ->set('file_prefix', $form_state->getValue('file_prefix'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Form element validation handler for the 'file_prefix' element.
   *
   * Only allow letters, numbers, underscores and dashes.
   */
  public static function validateFilePrefixElement($element, FormStateInterface $form_state, $form) {
    $file_prefix = trim($element['#value']);
    $file_prefix = str_replace(' ', '-', $file_prefix);
    $form_state->setValueForElement($element, $file_prefix);

    if (!preg_match('/^[a-zA-Z0-9-_]+$/', $file_prefix)) {
      $form_state->setError($element, t('Allowed characters are a-z, 0-9, \'-\', and \'_\'.'));
      return;
    }
  }

}
