<?php

namespace Drupal\book_pdf\Controller;

use Drupal\book_pdf\BookPdfGenerator;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\Messenger;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Book PDF routes.
 */
class BookPdfController extends ControllerBase {


  /**
   * The book PDF generator service.
   *
   * @var \Drupal\book_pdf\BookPdfGenerator
   */
  protected $generator;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * BookPdfController constructor.
   *
   * @param \Drupal\book_pdf\BookPdfGenerator $generator
   *   The book PDF generator service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   */
  public function __construct(BookPdfGenerator $generator, Messenger $messenger) {
    $this->generator = $generator;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('book_pdf.generator'),
      $container->get('messenger')
    );
  }

  /**
   * Clean a string so it can be used as a file name.
   *
   * @param string $string
   *   The string to clean.
   *
   * @return string
   *   The cleaned string.
   */
  protected function cleanString($string) {
    // Lower case everything.
    $string = strtolower($string);
    // Make alphanumeric (removes all other characters).
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    // Clean up multiple dashes or whitespaces.
    $string = preg_replace("/[\s-]+/", " ", $string);
    // Convert whitespaces and underscore to dash.
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
  }

  /**
   * Get a filename for the PDF based on the book title and date/time.
   *
   * @param \Drupal\node\Entity\Node $book
   *   The book node by which we can generate the filename from.
   *
   * @return string
   *   A string which can be used as the PDF attachment filename.
   *
   * @throws \Exception
   */
  protected function getFileName(Node $book) {
    $file_prefix = \Drupal::config('book_pdf.settings')->get('file_prefix');
    return $this->cleanString($file_prefix . trim($book->getTitle())) . '.pdf';
  }
  
  /**
   * Returns the Book PDF attachment response.
   */
  public function sendPdf(Request $request, Node $book) {
    if (($filePath = $this->generator->getFileUri($book))) {
      $response = new BinaryFileResponse($filePath, 200, ['Content-Type' => 'application/pdf']);
      $response->setContentDisposition('attachment', $this->getFileName($book));
    }
    else {
      \Drupal::messenger()->addError('Sorry, we were unable to download retrieve the PDF at this time. Please try again later.');
      return new RedirectResponse($request->server->get('HTTP_REFERER'));
    }
    return $response;

  }

}
